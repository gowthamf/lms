
package Code;

public class Reserved {
    private int ReserveId;
    private int BookId;
    private int MemberId;
    private String BookName;
    private int ReservedCount;

    public Reserved(int ReserveId, int BookId, int MemberId, String BookName, int ReservedCount) {
        this.ReserveId = ReserveId;
        this.BookId = BookId;
        this.MemberId = MemberId;
        this.BookName=BookName;
        this.ReservedCount=ReservedCount;
    }

    public int getReservedCount() {
        return ReservedCount;
    }

    public void setReservedCount(int ReservedCount) {
        this.ReservedCount = ReservedCount;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String BookName) {
        this.BookName = BookName;
    }
    
    

    public int getReserveId() {
        return ReserveId;
    }

    public void setReserveId(int ReserveId) {
        this.ReserveId = ReserveId;
    }

    public int getBookId() {
        return BookId;
    }

    public void setBookId(int BookId) {
        this.BookId = BookId;
    }

    public int getMemberId() {
        return MemberId;
    }

    public void setMemberId(int MemberId) {
        this.MemberId = MemberId;
    }
    
    
}
