/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Code;
        
import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnections {
    
    Connection connection;
    
    public  void connectToDB(){
        String host = "jdbc:derby://localhost:1527/LMS";
        String username="root";
        String password="123";
        try {
            connection = DriverManager.getConnection( host, username, password );
        } catch (SQLException ex) {
            System.out.println( ex.getMessage( ) );
        }
    }
    
    public  ArrayList<Books> getBooks(){
        
        ArrayList<Books> booksList=new ArrayList<Books>();
        connectToDB();
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="SELECT * FROM BOOKS";
            ResultSet results=statemt.executeQuery(SQL);
            
            while(results.next()){
                int bookId=results.getInt("BookID");
                String bookName=results.getString("BookName");
                String author=results.getString("Author");
                int quantity=results.getInt("Quantity");
                
                booksList.add(new Books(bookId, bookName, author, quantity));
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return booksList;
    }
    
    public ArrayList<Member> getMembers(){
        
        ArrayList<Member> memberList=new ArrayList<Member>();
        connectToDB();
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="SELECT * FROM MEMBERS WHERE NAME<>'admin'";
            ResultSet results=statemt.executeQuery(SQL);
            
            while(results.next()){
                int memberId=results.getInt("MEMBERID");
                String name=results.getString("NAME");
                int contactNumber =results.getInt("CONTACTNUMBER");
                String address=results.getString("ADDRESS");
                
                memberList.add(new Member(memberId, name, contactNumber, address));
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return memberList;
    }
    
    public Member canLogin(String userName,String passWord){
        connectToDB();
        Member currentLogin=new Member();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="SELECT * FROM MEMBERS WHERE NAME='"+userName+"'AND PASSWORD='"+passWord+"'";
            ResultSet results=statemt.executeQuery(SQL);
            
            if(results.next()){
                currentLogin.setMemberId(results.getInt("MEMBERID"));
                currentLogin.setMemberName(results.getString("NAME"));
                return currentLogin;
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return currentLogin;
    }
    
    public boolean insertBorrowedBooks(int bookId, int memberId, String borrowedDate, String returnedDate){
        connectToDB();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="INSERT INTO ROOT.BORROWED (BOOKID, MEMBERID, BORROWDATE, RETURNEDDATE, RETURNED, OVERDUEDAYS, OVERDUEAMOUNT) \n" +
"	VALUES ("+bookId+", "+memberId+",'"+borrowedDate+"','"+returnedDate+"',false, 0, 0)";
            int results=statemt.executeUpdate(SQL);
            
            if(results==1){
                return true;
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return false;
    }
    
    public ArrayList<Borrowed> getBorrowDetails(int memberId){
        connectToDB();
        ArrayList<Borrowed> borrowedList=new ArrayList<>();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="select BORROWED.RETURNEDDATE, BORROWED.BORROWDATE,BOOKS.BOOKNAME,BORROWED.BORROWEDID from BORROWED,BOOKS where BORROWED.MEMBERID="
                    +memberId+"AND RETURNED=FALSE AND BORROWED.BOOKID=BOOKS.BOOKID";
            ResultSet results=statemt.executeQuery(SQL);
            
         while(results.next()){
             String borrowedDate=results.getString("BORROWDATE");
             String returnDate=results.getString("RETURNEDDATE");
             int borrowId=results.getInt("BORROWEDID");
             String bookName=results.getString("BOOKNAME");
             borrowedList.add(new Borrowed(borrowId, 0, memberId, returnDate, 0, 0, borrowedDate,bookName));
             
         }
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return borrowedList;
    }
    
    public boolean updateReturning(int memberId, int overdueDays, int overDueFees){
        connectToDB();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="UPDATE BORROWED SET RETURNED=FALSE, OVERDUEDAYS="+overdueDays+",OVERDUEAMOUNT="+overDueFees+" where MEMBERID="+memberId;
            int res=statemt.executeUpdate(SQL);
            
            if(res==1){
                return true;
            }
         
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
    public boolean updateBooks(int bookId, String authorName, int quantity){
        connectToDB();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="UPDATE BOOKS SET AUTHOR='"+authorName+"',QUANTITY="+quantity+" where BOOKID="+bookId;
            int res=statemt.executeUpdate(SQL);
            
            if(res==1){
                return true;
            }
         
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
    public boolean deleteBooks(int bookId){
        connectToDB();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="DELETE FROM BOOKS where BOOKID="+bookId;
            int res=statemt.executeUpdate(SQL);
            
            if(res==1){
                return true;
            }
         
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
    public ArrayList<Books> searchBook(String name){
        ArrayList<Books> booksList=new ArrayList<Books>();
        connectToDB();
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="SELECT * FROM BOOKS WHERE BOOKNAME LIKE '%"+name+"%'";
            ResultSet results=statemt.executeQuery(SQL);
            
            while(results.next()){
                int bookId=results.getInt("BookID");
                String bookName=results.getString("BookName");
                String author=results.getString("Author");
                int quantity=results.getInt("Quantity");
                
                booksList.add(new Books(bookId, bookName, author, quantity));
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return booksList;
    }
    
    public ArrayList<Borrowed> getOverDueBooks(){
        connectToDB();
        ArrayList<Borrowed> borrowedList=new ArrayList<>();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="select BORROWED.RETURNEDDATE, BORROWED.BORROWDATE,BOOKS.BOOKNAME,BORROWED.BORROWEDID \n" +
"from BORROWED,BOOKS WHERE RETURNED=FALSE AND BORROWED.BOOKID=BOOKS.BOOKID\n" +
"";
            ResultSet results=statemt.executeQuery(SQL);
            
         while(results.next()){
             String borrowedDate=results.getString("BORROWDATE");
             String returnDate=results.getString("RETURNEDDATE");
             int borrowId=results.getInt("BORROWEDID");
             String bookName=results.getString("BOOKNAME");
             borrowedList.add(new Borrowed(borrowId, 0, 0, returnDate, 0, 0, borrowedDate,bookName));
             
         }
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return borrowedList;
    }
    
    public boolean reserveBook(int bookId, int memberId){
        connectToDB();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="INSERT INTO ROOT.RESERVED (BOOKID, MEMBERID) VALUES ("+bookId+","+memberId+")";
            int results=statemt.executeUpdate(SQL);
            
            if(results==1){
                return true;
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return false;
    }
    
    public ArrayList<Reserved> getReservedBooks(){
        connectToDB();
        ArrayList<Reserved> reservedList=new ArrayList<>();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="select COUNT(RESERVED.RESERVEID) as ReserveCount,BOOKS.BOOKNAME from RESERVED,BOOKS WHERE RESERVED.BOOKID=BOOKS.BOOKID \n" +
"GROUP BY BOOKS.BOOKNAME";
            ResultSet results=statemt.executeQuery(SQL);
            
         while(results.next()){
             int count=results.getInt("ReserveCount");
             String bookName=results.getString("BOOKNAME");
             reservedList.add(new Reserved(0, 0, 0, bookName, count));           
         }
          
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return reservedList;
    }
    
    public ArrayList<Member> searchMember(String name){
        ArrayList<Member> memberList=new ArrayList<Member>();
        connectToDB();
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="SELECT * FROM MEMBERS WHERE NAME LIKE '%"+name+"%' AND NAME<>'admin'";
            ResultSet results=statemt.executeQuery(SQL);
            
            while(results.next()){
                String memberName=results.getString("NAME");

                memberList.add(new Member(0, memberName, 0, null));
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return memberList;
    }
    
    public int registerMember(String name, String password, int contactNo, String address){
        int memberId=0;
        connectToDB();
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="INSERT INTO MEMBERS (\"NAME\", PASSWORD, CONTACTNUMBER, ADDRESS) \n" +
"	VALUES ('"+name+"','"+password+"','"+contactNo+"','"+address+"')";
            int results=statemt.executeUpdate(SQL,Statement.RETURN_GENERATED_KEYS);
            ResultSet rs=statemt.getGeneratedKeys();
            
            if(rs.next()){
                memberId=rs.getInt(1);
            }
            
//            if(rs.next()){
//                memberId=rs.getInt(1);
//            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return memberId;
    }
    
    public boolean insertBook(String bookName, String author, int quantity){
        connectToDB();
        
        Statement statemt;
        try {
            statemt = connection.createStatement();
            String SQL="INSERT INTO BOOKS (BOOKNAME, AUTHOR, QUANTITY) VALUES ('"+bookName+"','"+author+"',"+quantity+")";
            int results=statemt.executeUpdate(SQL);
            
            if(results==1){
                return true;
            }
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return false;
    }
}
