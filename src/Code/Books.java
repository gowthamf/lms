
package Code;

public class Books {
    private int BookId;
    private String BookName;
    private String Author;
    private int Quantity;

    public Books(int BookId, String BookName, String Author, int Quantity) {
        this.BookId = BookId;
        this.BookName = BookName;
        this.Author = Author;
        this.Quantity = Quantity;
    }

    public int getBookId() {
        return BookId;
    }

    public void setBookId(int BookId) {
        this.BookId = BookId;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String BookName) {
        this.BookName = BookName;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String Author) {
        this.Author = Author;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    @Override
    public String toString() {
        return "BookName=" + BookName + ", Quantity=" + Quantity;
    }
}
