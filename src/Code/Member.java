
package Code;

public class Member {
    private int MemberId;
    private String MemberName;
    private int ContactNumber;
    private String Address;

    public Member(int MemberId, String MemberName, int ContactNumber, String Address) {
        this.MemberId = MemberId;
        this.MemberName = MemberName;
        this.ContactNumber = ContactNumber;
        this.Address = Address;
    }

    public Member() {
    }
    
    

    public int getMemberId() {
        return MemberId;
    }

    public void setMemberId(int MemberId) {
        this.MemberId = MemberId;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String MemberName) {
        this.MemberName = MemberName;
    }

    public int getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(int ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }
    
    
}
