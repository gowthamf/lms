
package Code;


public class Borrowed {
    private int BorrowId;
    private int BookId;
    private int MemberId;
    private String ReturnedDate;
    private int OverDueDays;
    private int OverDueAmount;
    private String BorrowedDate;
    private String BookName;

    public Borrowed(int BorrowId, int BookId, int MemberId, String ReturnedDate, int OverDueDays, int OverDueAmount, String BorrowedDate, String BookName) {
        this.BorrowId = BorrowId;
        this.BookId = BookId;
        this.MemberId = MemberId;
        this.ReturnedDate = ReturnedDate;
        this.OverDueDays = OverDueDays;
        this.OverDueAmount = OverDueAmount;
        this.BorrowedDate=BorrowedDate;
        this.BookName=BookName;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String BookName) {
        this.BookName = BookName;
    }

    public Borrowed() {
    }

    public String getBorrowedDate() {
        return BorrowedDate;
    }

    public void setBorrowedDate(String BorrowedDate) {
        this.BorrowedDate = BorrowedDate;
    }
    

    public int getBorrowId() {
        return BorrowId;
    }

    public void setBorrowId(int BorrowId) {
        this.BorrowId = BorrowId;
    }

    public int getBookId() {
        return BookId;
    }

    public void setBookId(int BookId) {
        this.BookId = BookId;
    }

    public int getMemberId() {
        return MemberId;
    }

    public void setMemberId(int MemberId) {
        this.MemberId = MemberId;
    }

    public String getReturnedDate() {
        return ReturnedDate;
    }

    public void setReturnedDate(String ReturnedDate) {
        this.ReturnedDate = ReturnedDate;
    }

    public int getOverDueDays() {
        return OverDueDays;
    }

    public void setOverDueDays(int OverDueDays) {
        this.OverDueDays = OverDueDays;
    }

    public int getOverDueAmount() {
        return OverDueAmount;
    }

    public void setOverDueAmount(int OverDueAmount) {
        this.OverDueAmount = OverDueAmount;
    }
    
    
}
